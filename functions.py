import os
import re
import numpy as np
from astropy.coordinates import get_sun, get_icrs_coordinates
from astropy.time import Time
from datetime import datetime
from astropy.coordinates import SkyCoord
from astropy import units as u
import pandas as pd
import pickle
import sys
from tqdm import tqdm
from scipy.interpolate import griddata, LinearNDInterpolator
from scipy.ndimage import gaussian_filter
import numpy as np
import pickle
import pandas as pd
from astropy.time import Time
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import argparse
from pathlib import Path
import json
from astropy.io import fits
from matplotlib.widgets import Slider
import socket

    
def validity_arg_RA(value):
    RA = float(value) 
    if RA < 0 or RA > 360:
        raise argparse.ArgumentTypeError('RA must be between 0 and 360 degrees')
    return RA

def validity_arg_Dec(value):
    Dec = float(value) 
    if Dec < -90 or Dec > 90:
        raise argparse.ArgumentTypeError('Dec must be between -90 and 90 degrees')
    return Dec

def validity_arg_obs(value):
    date_pattern = re.compile(r'\d{1,2}-[A-Za-z]{3}')
    
    if date_pattern.match(value) or value == '':
        return value
    else:
        raise argparse.ArgumentTypeError('Invalid date format. Use the format \'DD-Mon\', e.g., \'23-Aug\'.')
    
def validity_arg_SEA(value, min_sea):
    sea = int(value) 
    if sea < min_sea:
        raise argparse.ArgumentTypeError(f'Sorry, but CHEOPS can look that close to the Sun, please choose a SEA value >= {min_sea} and <= 120')
    if sea > 120:
        raise argparse.ArgumentTypeError(f'CHEOPS has never had a Sun Exclusion Angle larger than 120! Please choose a SEA value >= {min_sea} and <= 120')
    return sea

def validity_arg_orb_angle(value):
    max_roll_rate_angle = int(value) 
    if max_roll_rate_angle <= 0:
        raise argparse.ArgumentTypeError('Please choose a angle_max_roll > 0.')
    if max_roll_rate_angle > 80:
        raise argparse.ArgumentTypeError('Please choose a angle_max_roll < 80.')
    return max_roll_rate_angle

def validity_file_entries(target,id):
    
    valid_entry = True
    coord_entry = False
   
    if type(target['target_name']) == float:
        target['target_name'] = 'Target_'+str(id)
        print(f'No ID given for target # {id}, setting name to: \'Target_{id}\'.')
        
    target_name = target['target_name']
    
    RA = target['RA(deg)']
    if (RA > 0) & (RA < 360):
        coord_entry = True
        
    DEC = target['DEC(deg)']
    if (DEC > -90) & (DEC < 90):
        coord_entry = True
    
    if coord_entry == False:
        print(f'{target_name}: RA or Dec is either not given or wrong, querying RA/Dec')
        try:
            coo = get_icrs_coordinates(target_name)
        except:
            valid_entry = False
            target['obs(dd-Mon)'] = '' # set to default, but target will be ignored anyway
            print(f'{target_name}: Invalid target_name, skipping.')
            return valid_entry, target
        target['RA(deg)'] = coo.ra.value
        target['DEC(deg)'] = coo.dec.value
    
    planned_obs = str(target['obs(dd-Mon)'])
    date_pattern = re.compile(r'\d{1,2}-[A-Za-z]{3}')
    if (not date_pattern.match(planned_obs)) or (len(planned_obs) > 6):
        target['obs(dd-Mon)'] = ''
        print(f'{target_name}: Empty planned_obs or invalid date format, planned observation date not set.')
        
    return valid_entry, target

def read_merge_star_obs_and_target_list(star_obs_file_path, target_list_file_path):
    
    star_obs_columns = ['target_name','Programme','Period','GO','Pri','Max','Num','Visit_Id','Start_Date_raw','Start_Time_raw','End_Date_raw','End_Time_raw','Duration','Eff_Duration','Flex_Duration']
    star_obs = pd.read_csv(star_obs_file_path, delimiter = '\s+', engine='python', skiprows=10, names=star_obs_columns)
    
    # Reformate the time columns 
    star_obs['Start_Date'] = star_obs['Start_Date_raw'] + 'T' + star_obs['Start_Time_raw']
    star_obs['End_Date']  = star_obs['End_Date_raw'] + 'T' + star_obs['End_Time_raw']

    # Convert to julian date to have numeric values
    star_obs['Start_Date'] = Time(pd.to_datetime(star_obs['Start_Date']), format='datetime64', scale='utc').jd
    star_obs['End_Date'] = Time(pd.to_datetime(star_obs['End_Date']), format='datetime64',scale='utc').jd
    
    star_obs['Mid_Time'] = np.round((star_obs['Start_Date']+star_obs['End_Date'])/2)

    # # Add the number of sideral years from the original year
    # current_year = datetime.now().year
    # years_since_calculations = current_year - year_calc
    # star_obs['Start_Date'] += sideral_year*years_since_calculations
    # star_obs['End_Date'] += sideral_year*years_since_calculations
        
    # In the case there are one occurence of start on 31-DEC and ends on 01-JAN, then move the start to 31-DEC of the previous year
    # DEC_31_start = np.where((star_obs['Start_Date_raw'].str[:6] == '31-DEC') & (star_obs['End_Date_raw'].str[:6] == '01-JAN'))[0]
    # star_obs.loc[DEC_31_start, 'Start_Date'] = star_obs.loc[DEC_31_start, 'Start_Date'] - sideral_year


    # Convert all Start/End dates back to current year
    # current_year = str(datetime.now().year)
    # previous_year = str(datetime.now().year-1)
    # day_month_start = star_obs['Start_Date_raw'].str[:6]
    # star_obs['Start_Date'] = day_month_start + '-' + current_year + 'T' + star_obs['Start_Time_raw']
    # day_month_end = star_obs['End_Date_raw'].str[:6]
    # star_obs['End_Date'] = day_month_end + '-' + current_year + 'T' + star_obs['End_Time_raw']
    # # In the case there are one occurence of start on 31-DEC and ends on 01-JAN, then move the start to 31-DEC of the previous year
    # DEC_31_start = np.where((star_obs['Start_Date_raw'].str[:6] == '31-DEC') & (star_obs['End_Date_raw'].str[:6] == '01-JAN'))[0]
    # star_obs.loc[DEC_31_start, 'Start_Date'] = star_obs.loc[DEC_31_start, 'Start_Date'].str[:6] + '-' + previous_year
    
    # # Convert to julian date to have numeric values
    # star_obs['Start_Date'] = Time(pd.to_datetime(star_obs['Start_Date']), format='datetime64', scale='utc').jd
    # star_obs['End_Date'] = Time(pd.to_datetime(star_obs['End_Date']), format='datetime64',scale='utc').jd
    
    # Compute efficiency
    star_obs['Efficiency'] = (star_obs['Eff_Duration']*100)/star_obs['Duration'] 
    # Keep only necessary columns
    desired_columns = ['target_name', 'Start_Date', 'End_Date', 'Efficiency', 'Mid_Time']
    star_obs = star_obs[desired_columns]
    
    ## Target list file ##
    target_list_columns = ['target_name','RA(h:m:s)','RA(deg)','Dec(d:m:s)','Dec(deg)','Period(days)','T_c(HJD)','Mag','SpT','Programme','GO','Prty','N_Visits','Exposure_Time','Visit_Duration','PM_RA','PM_DEC','Parallax','Minimum_Effective_Duration_Per_Orbit','Earliest_Start_Date','Latest_End_Date','Earliest_Start_Phase','Latest_Start_Phase','CPRs']
    target_list = pd.read_csv(target_list_file_path, delimiter = '\s+', engine='python', skiprows=1, names=target_list_columns)
    # Keep only necessary columns
    desired_columns = ['target_name','RA(deg)','Dec(deg)']
    target_list = target_list[desired_columns]
    
    ## Merge datasets ##
    column_to_merge_on = 'target_name'
    data = pd.merge(star_obs, target_list, on=column_to_merge_on, how='inner')

    return data

def RA_Dec_interpolation(data, interp_grid_size_deg, max_eff):
    
    # ra/dec/eff
    ra = data['RA(deg)']
    dec = data['Dec(deg)']
    eff = data['Efficiency']
    
    # Create a grid for interpolation
    deg_int = interp_grid_size_deg
    interp_RA = np.linspace(0, 360, int(360/deg_int))
    interp_Dec = np.linspace(-90, 90, int(180/deg_int))
    interp_ra_grid, interp_dec_grid = np.meshgrid(interp_RA, interp_Dec)
    
    # gridata interpolation
    interp_values = griddata((ra,dec), eff, (interp_ra_grid, interp_dec_grid), method='linear', fill_value = 0)    
    
    # set all values above the max value found in the data (which is a hard limit defined by the max efficiency imposed by the SAA) to that max value
    interp_values[np.where(interp_values > max_eff)] = max_eff
    
    return data['Efficiency'], interp_ra_grid, interp_dec_grid, interp_values, interp_RA, interp_Dec 
    
def mask_zone(interp_ra_grid, interp_dec_grid, interp_values, cone_center = 'sun', start_zone = 0, end_zone = 60, day = Time('2024-01-01', format='iso', scale='utc'), single_target = False):
    
    values = interp_values.copy()
    
    # Define cone center
    if cone_center == 'sun':
        cone_ra  = get_sun(Time(day, format='jd', scale='utc')).ra.value  # Sun ra
        cone_dec = get_sun(Time(day, format='jd', scale='utc')).dec.value # Sun dec
    # elif cone_center == 'antisun':
    #     cone_ra  = (get_sun(Time(day, format='jd', scale='utc')).ra.value + 180.)%360  # Antisun ra
    #     cone_dec = get_sun(Time(day, format='jd', scale='utc')).dec.value * (-1)       # Antisun dec
    elif cone_center == 'normal_orb_plane':
        cone_ra  = (get_sun(Time(day, format='jd', scale='utc')).ra.value + 180.)%360  # Antisun ra
        cone_dec = -8 # CHEOPS inclination = 82 degrees --> 0-90+82 = -8
    else:
        raise ValueError("Cone center need to be 'sun' or 'normal_orb_plane'")
    
    # Create a SkyCoord object for cone center
    coords_cone_center = SkyCoord(ra=cone_ra*u.deg, dec=cone_dec*u.deg, frame='icrs')

    # For the interpolation
    interp_coords = SkyCoord(ra=interp_ra_grid*u.deg, dec=interp_dec_grid*u.deg, frame='icrs')
    
    # Mask for points within the cone
    if single_target:
        if (coords_cone_center.separation(interp_coords).degree < start_zone) | (coords_cone_center.separation(interp_coords).degree > end_zone):
        # if cone_center.separation(interp_coords).degree > cone_radius:
            values = 0
    else:
        # interp_mask = cone_center.separation(interp_coords).degree > cone_radius
        # interp_values[interp_mask] = np.nan
        interp_mask = (coords_cone_center.separation(interp_coords).degree < start_zone) | (coords_cone_center.separation(interp_coords).degree > end_zone)
        values[interp_mask] = np.nan

    return values

def define_orbital_plane(day = Time('2024-01-01', format='iso', scale='utc')):

    # Specify cone parameters
    orb_plane_normal_ra = (get_sun(Time(day, format='jd', scale='utc')).ra.value + 180.)%360  # Antisun ra
    orb_plane_normal_dec = -8 # Normal to CHEOPS' orbital plane

    # Create a SkyCoord object for the normal of the orbital plane
    orb_plane_normal = SkyCoord(ra=orb_plane_normal_ra*u.deg, dec=orb_plane_normal_dec*u.deg, frame='icrs')
    
    return orb_plane_normal
    
def get_orbital_plane_dist(target_ra, target_dec, day = Time('2024-01-01', format='iso', scale='utc')):
      
    # Target position
    target_position = SkyCoord(ra=target_ra*u.deg, dec=target_dec*u.deg, frame='icrs')
    
    # Distance from target to normal of the orbital plane
    orb_plane_normal = define_orbital_plane(day)
    dist_to_target = orb_plane_normal.separation(target_position).degree
    
    # Angle between target and orbital plane
    dist_to_orb_plane = np.abs(90-dist_to_target)
    
    return dist_to_orb_plane

def find_JWST_vis_ranges(arr, dates):
    sequences = []
    in_sequence = False
    for i, value in enumerate(arr):
        if value > 0 and not in_sequence:
            start = i
            in_sequence = True
        elif value == 0 and in_sequence:
            end = i
            sequences.append((start, end))
            in_sequence = False
    if in_sequence: # append the last part if the last item is one
        sequences.append((start, len(arr)))
        
    vis_periods = []
    for start,end in sequences:
        vis_periods.append((dates[start],dates[end-1]))
    return vis_periods

def check_JWST_visibility_single_target(ra_target, dec_target, day):
    
    coords = SkyCoord(ra=ra_target*u.deg, dec=dec_target*u.deg, frame='icrs')      
    
    sun_gcrs = get_sun(Time(day, format='jd', scale='utc'))
    sun_pos = SkyCoord(ra=sun_gcrs.ra, dec=sun_gcrs.dec)
    
    JWST_min_sep = 85*u.deg      # Minimum angle to sun for JWST target
    JWST_max_sep = 135*u.deg     # Maximum angle to sun for JWST target

    if (sun_pos.separation(coords).degree > JWST_min_sep.value) & (sun_pos.separation(coords).degree < JWST_max_sep.value):
        return 1
    else:
        return 0

def add_orbital_plane(interp_data):
    
    n_points = 1000  # number of points to approximate the circle
    angles = np.linspace(0, 2 * np.pi, n_points)
    
    interp_data["orbital_plane_ras"] = pd.Series([np.zeros(shape = np.shape(angles))] * len(interp_data), index=interp_data.index)
    interp_data["orbital_plane_decs"] = pd.Series([np.zeros(shape = np.shape(angles))] * len(interp_data), index=interp_data.index)
    
    for index,row in interp_data.iterrows(): 
        day = row.date
        current_day = Time(day, format='jd', scale='utc')
        orb_plane_normal = define_orbital_plane(current_day)
        orbital_plane = orb_plane_normal.directional_offset_by(angles * u.rad, 90 * u.deg)
        interp_data.at[index, "orbital_plane_ras"] = orbital_plane.ra.deg
        interp_data.at[index, "orbital_plane_decs"] = orbital_plane.dec.deg

    return interp_data

def search_files(folder_path, search_string):
   
    # List all files in the specified folder
    files = os.listdir(folder_path)
    
    files_w_str = []

    # Filter files containing the search string
    for file in files:
        file_path = os.path.join(folder_path, file)
        if os.path.isfile(file_path) and search_string in file:
            files_w_str.append(file_path)
    return files_w_str
        
def full_interp(processing_folder, target_list, star_obs_vs_time, Gmag, min_SEA, files_version, time_array, sun_angle_grid, ra_grid, dec_grid, current_year):
    
    print(f'Interpolating for Gmag = {Gmag} ...')

    data = read_merge_star_obs_and_target_list(star_obs_vs_time, target_list)
    
    # Create dataframe that will contain everything
    columns = ['date', 'eff_data', 'interp_ra_grid', 'interp_dec_grid', 'interp_values']


    ### INTERPOLATION ###
    
    sun_angle = get_sun(Time(data['Mid_Time'].values, format='jd', scale='utc')).ra.value
    data['Sun_Angle'] = sun_angle

    min_sun_angles = data[data['Sun_Angle'] < 20]# data['Sun_Angle'].min()]
    max_sun_angles = data[data['Sun_Angle'] > 340]# data['Sun_Angle'].max()]
    
    min_ra_angles = data[data['RA(deg)'] < 150] #== data['RA(deg)'].min()]
    max_ra_angles = data[data['RA(deg)'] > 210] #== data['RA(deg)'].max()]
    
    sun_angle_wrapped = np.hstack([data['Sun_Angle'] , 
                                   min_sun_angles['Sun_Angle'] + 360, max_sun_angles['Sun_Angle'] - 360, # Wrap around the sun angle
                                   min_ra_angles['Sun_Angle'], max_ra_angles['Sun_Angle'] # Add the values for the wrap around on ra
                                   ])
    
    ra_wrapped = np.hstack([data['RA(deg)'], 
                            min_sun_angles['RA(deg)'], max_sun_angles['RA(deg)'], # Add the values for the wrap around on sun_angle
                            min_ra_angles['RA(deg)'] + 360, max_ra_angles['RA(deg)'] - 360 # Wrap around ra
                            ])
    
    dec_wrapped = np.hstack([data['Dec(deg)'], 
                            min_sun_angles['Dec(deg)'], max_sun_angles['Dec(deg)'], # Add the values for the wrap around on sun_angle
                            min_ra_angles['Dec(deg)'] , max_ra_angles['Dec(deg)'] # Add the values for the wrap around on ra
                            ])
    
    efficiency_wrapped = np.hstack([data['Efficiency'], 
                            min_sun_angles['Efficiency'], max_sun_angles['Efficiency'], # Add the values for the wrap around on sun_angle
                            min_ra_angles['Efficiency'] , max_ra_angles['Efficiency'] # Add the values for the wrap around on ra
                            ])
    
    points = np.array([sun_angle_wrapped, ra_wrapped, dec_wrapped]).T # combine to 3D array

    interpolator = LinearNDInterpolator(points, efficiency_wrapped)

    # Interpolate on full grid #
    sun_angle_mesh, ra_mesh, dec_mesh = np.meshgrid(sun_angle_grid, ra_grid, dec_grid, indexing='ij')
    
    interp_points = np.column_stack([
        sun_angle_mesh.ravel(),
        ra_mesh.ravel(),
        dec_mesh.ravel()
    ])
    
    # Interpolate in chunks to show progress
    chunk_size = int(np.round(len(interp_points)/100,-1))
    interpolation_result = []
    for i in tqdm(range(0, len(interp_points), chunk_size), desc="Interpolating",unit="%"):
        chunk = interp_points[i:i+chunk_size]
        interpolation_result.append(interpolator(chunk))

    grid_efficiency = np.concatenate(interpolation_result)    
    grid_efficiency = grid_efficiency.reshape(len(sun_angle_grid), len(ra_grid), len(dec_grid))

    columns = ['date', 'interp_values']
    full_interp_data = pd.DataFrame(columns=columns)

    # Mask sun Exclusion angle
    for day, efficiency_day in zip(time_array,grid_efficiency):
        DEC, RA = np.meshgrid(dec_grid, ra_grid)
        EFF = efficiency_day.reshape(len(ra_grid), len(dec_grid))
        # apply sun exclusion angle
        EFF = mask_zone(RA, DEC, EFF, cone_center = 'sun', start_zone = min_SEA, end_zone = 180, day = day, single_target = False)
        temp_df = pd.DataFrame([[day, EFF]], columns=columns)
        full_interp_data = pd.concat([full_interp_data, temp_df], ignore_index=True)

    # full_interp_data = add_JWST_vis(full_interp_data, RA, DEC)
    
    full_interp_data = add_orbital_plane(full_interp_data)
    
    # full_interp_data = add_orb_plane_zone(full_interp_data, RA, DEC, max_angle_max_roll_rate)
        
    with open(os.path.join(processing_folder, 'interp_function_Gmag' + str(Gmag) + '_' + str(current_year) + '_V' + files_version), 'wb') as file:
        pickle.dump(interpolator, file)
        
    with open(os.path.join(processing_folder, 'time_eff_interpolated_table_Gmag' + str(Gmag) + '_' + str(current_year) + '_V' + files_version), 'wb') as file:
        full_interp_data.to_pickle(file)
  
def interactive_plot(current_folder, args, full_interp_data, RA, DEC, targets, eff_uncertainty, Gmag_user):
    # Plot 2: Interactive visibility map
  
    interval_day = 1
    full_interp_data = full_interp_data.iloc[::interval_day].reset_index(drop=True)

    # Import Kepler field
    kepler_field = pd.read_table(os.path.join(current_folder, 'misc_files/Kepler_field.txt'), sep = '\s+', header = 3)
    # Import K2 field
    k2_field = json.load(open(os.path.join(current_folder, 'misc_files/k2_field.json')))
    # Import the PLATO field
    with fits.open(os.path.join(current_folder,"misc_files/lops2_healpix9_footprint.fits")) as hdul:
        PLATO_data = hdul[1].data

    # Min value for colorbar
    # min_values = [np.nanmin(array) for array in full_interp_data['interp_values'].values]
    # min_value_cbar = int(np.min(min_values)//10*10) # round to the decade below
    min_value_cbar = 40
    levels = range(min_value_cbar,101,1)
    cbar_range = range(min_value_cbar,101,10)    
    
    fig, ax = plt.subplots(ncols = 1,figsize=(12,6))
    plt.subplots_adjust(left= 0.1, bottom= 0.15, right= 1, top= 0.93, wspace= 0, hspace= 0)
    ax_contourf = ax.twinx()
    ax_contourf.get_yaxis().set_ticks([])
    ax_contourf.set_yticklabels([])

    ### Plot initial data ###
    row = full_interp_data.loc[0]
    init_day = row['date']
    
    # Plot visibility cone    
    # apply SEA zone to data
    for i, row in full_interp_data.iterrows():
        day = row['date']
        full_interp_data.at[i, 'interp_values'] = mask_zone(RA, DEC, row['interp_values'], cone_center = 'sun', start_zone = args.SEA, end_zone = 180, day = day, single_target=False)
    # Plot visibility 
    eff_contours = ax_contourf.contourf(RA,DEC, full_interp_data.loc[0]['interp_values'], levels=levels, alpha = 0.8)

    # Plot target(s) location
    nb_max_targets = 50
    if len(targets) < nb_max_targets :
        for id, target in targets.iterrows():
            target_name = target['target_name']
            RA_target = target['RA(deg)']
            Dec_target = target['DEC(deg)']
            ax.scatter(RA_target,Dec_target, marker = '*', s = 200, edgecolor='k', zorder = 10, label = target_name)   
            ax_contourf.scatter(RA_target,Dec_target, marker = '*', s = 200, edgecolor='k', zorder = 10) 
    else:
        print(f'\nMore than {nb_max_targets} targets, not showing them on visibility map.\n')
        
    fig.colorbar(eff_contours, ax=ax_contourf, label = f'Efficiency (+/-{eff_uncertainty} %) over 7 orbits (~12 hours), for Gmag = {Gmag_user}', ticks=cbar_range)
    
    # Plot initial JWST visibility 
    matplotlib.rcParams['hatch.linewidth'] = 0.5  # previous pdf hatch linewidth
    matplotlib.rcParams['hatch.color'] = 'lightgray'  # previous pdf hatch linewidth
    JWST_vis_init = np.ones(shape = np.shape(RA))
    JWST_vis = mask_zone(RA, DEC, JWST_vis_init, cone_center = 'sun', start_zone = 85, end_zone = 135, day = init_day, single_target=False)
    ax_contourf.contourf(RA,DEC, JWST_vis, levels=[0.5,1], colors=['seagreen','none'], hatches=['//',''], label = 'JWST visibility', alpha = 0.05, zorder = 5)
        
    # Plot orbital plane
    if args.orb_plane:
        # Plot orbital plane 
        ind_orb_plane_line = np.argsort(row['orbital_plane_ras'])
        orb_plane = ax_contourf.plot(row['orbital_plane_ras'][ind_orb_plane_line],row['orbital_plane_decs'][ind_orb_plane_line],'-', lw = 1, color = 'k', label = 'CHEOPS orbital plane')
    if args.angle_max_roll > 0:
        # Plot zone around orbital plane where roll angle is max
        orb_plane_zone_init = np.ones(shape = np.shape(RA))
        start_orb_plane_zone = 90 - args.angle_max_roll
        end_orb_plane_zone = 90 + args.angle_max_roll
        orb_plane_zone = mask_zone(RA, DEC, orb_plane_zone_init, cone_center = 'normal_orb_plane', start_zone = start_orb_plane_zone, end_zone = end_orb_plane_zone, day = init_day, single_target=False)
        orb_plane_zone_vis_cone = mask_zone(RA, DEC, orb_plane_zone, cone_center = 'sun', start_zone = args.SEA, end_zone = 180, day = init_day, single_target=False)
        ax_contourf.contourf(RA,DEC, orb_plane_zone_vis_cone, levels=[0.99,1], colors=['indianred','none'], hatches=['',''], label = 'zone close to the orbital plane', alpha = 0.3, zorder = 5)

    # Plot the Kepler field
    for mod_value in kepler_field['mod'].unique():
        for out_value in kepler_field['out'].unique():
            curr_mod = kepler_field[(kepler_field['out'] == out_value) & (kepler_field['mod'] == mod_value)]
            ndf = curr_mod.copy()
            ndf = ndf.reset_index()
            ndf = ndf.drop(2)
            # invert last 2 rows to have the points around trhe square in order
            ras = [ndf.iloc[0]['RA'],ndf.iloc[1]['RA'],ndf.iloc[3]['RA'],ndf.iloc[2]['RA']]
            decs = [ndf.iloc[0]['DEC'],ndf.iloc[1]['DEC'],ndf.iloc[3]['DEC'],ndf.iloc[2]['DEC']]
            if (mod_value == 24) & (out_value == 4):
                ax.fill(ras,decs, color = 'darkorange', alpha = 0.2, label = 'Kepler field')
            else:
                ax.fill(ras,decs, color = 'darkorange', alpha = 0.2)

    # Plot the K2 fields
    for campaign in k2_field.keys(): # iterate through K2 fields
        for channel in k2_field[campaign]['channels'].keys():
            curr_channel = k2_field[campaign]["channels"][channel]
            if (campaign == 'c20') & (channel == '84'):
                ax.fill(curr_channel["corners_ra"] + curr_channel["corners_ra"][:1],
                curr_channel["corners_dec"] + curr_channel["corners_dec"][:1], color = 'grey', alpha = 0.2, label = 'K2 fields')
            else:
                ax.fill(curr_channel["corners_ra"] + curr_channel["corners_ra"][:1],
                curr_channel["corners_dec"] + curr_channel["corners_dec"][:1], color = 'grey', alpha = 0.2)
                
    # # Plot the PLATO field 
    ax.tricontourf(PLATO_data.ra, PLATO_data.dec, PLATO_data.ncam, cmap='cividis', alpha = 0.3, label = 'PLATO field')
                
    # Manual legend labels
    legend_labels = ['PLATO field (6 cameras)', 'PLATO field (12 cameras)','PLATO field (18 cameras)','PLATO field (24 cameras)', 'JWST visibility', 'Maximum roll rate', 'CHEOPS orbital plane']
    legend_handles = []
    # Colors used
    cmap = plt.get_cmap('cividis')
    norm = matplotlib.colors.Normalize(vmin=np.min(PLATO_data.ncam), vmax=np.max(PLATO_data.ncam))
    colors = [cmap(norm(val), alpha=0.5) for val in np.unique(PLATO_data.ncam)]
    # Create proxy artists for each label
    for ii,label in enumerate(legend_labels):
        if label == 'JWST visibility': # JWST legend
            handle = ax.fill(RA_target,Dec_target+2,color='seagreen', alpha = 0.2, label = label, hatch = '////', linewidth = 1)
            legend_handles.append(handle)  
        elif "PLATO" in label: # PLATO field
            handle = ax.fill(RA_target,Dec_target+2,color=colors[ii], label = label)
            legend_handles.append(handle)
        elif (args.angle_max_roll) and (label == 'Maximum roll rate'): # Max roll rate zone
            handle = ax.fill(RA_target,Dec_target+2,color='indianred', alpha = 0.5, label = label, hatch = '', linewidth = 1)
            legend_handles.append(handle)  
        elif (args.orb_plane) and (label == 'CHEOPS orbital plane'): # CHEOPS orbital plane
            handle = ax.plot(RA_target, Dec_target + 2, color='black', linewidth=1, label=label)
            legend_handles.append(handle)  
        else:
            pass
    # Add existing plot handles to the legend
    existing_handles, existing_labels = ax.get_legend_handles_labels()
    # Combine handles and labels
    all_handles = existing_handles + legend_handles
    all_labels = existing_labels + legend_labels
    #plot legend
    ax.legend(handles=all_handles, labels=all_labels, loc='lower right')  # Adjust the location as needed

    ax.grid()     
    ax.set_axisbelow(True)
    ax_contourf.set_title(Time(row['date'], format='jd', scale='utc').strftime('%d-%b')+ ' (use slider below the graph to change the date)', weight = 'bold')
    ax.set_ylabel('Dec [deg]', fontweight = 'bold')
    ax.set_xlabel('RA [deg]', fontweight = 'bold')
    ax.set_xlim([0,360])
    ax.set_ylim([-90,90])

    # Make a horizontal slider to control the frequency.
    min_date = full_interp_data.iloc[0].date
    max_date = full_interp_data.iloc[-1].date
    axfreq = fig.add_axes([0.1, 0.01, 0.75, 0.05])
    date_slider = Slider(
        ax=axfreq,
        label='Date',
        valmin=min_date,
        valmax=max_date,
        valinit=min_date,
        valstep = interval_day,
        valfmt = '',
        facecolor='indianred', 
        edgecolor='k'
    )
            
    # The function to be called anytime a slider's value changes
    def update(val, eff_contours):
        date = date_slider.val
        eff_at_date = full_interp_data.loc[full_interp_data['date'] == date]
        
        ax_contourf.clear()

        # Plot Visibility cone
        ax_contourf.contourf(RA,DEC, eff_at_date['interp_values'].iloc[0], levels=levels, alpha = 0.8)
            
        # Plot JSWT zone #
        JWST_vis = mask_zone(RA, DEC, JWST_vis_init, cone_center = 'sun', start_zone = 85, end_zone = 135, day = eff_at_date['date'].iloc[0], single_target=False)
        ax_contourf.contourf(RA,DEC, JWST_vis, levels=[0.5,1], colors=['seagreen','none'], hatches=['//',''], label = 'JWST visibility', alpha = 0.05, zorder = 5)
        # Plot orbital plane and zone #
        if args.orb_plane:
            # Plot orbital plane 
            ind_orb_plane_line = np.argsort(eff_at_date['orbital_plane_ras'].iloc[0]) # orbital plane
            ax_contourf.plot(eff_at_date['orbital_plane_ras'].iloc[0][ind_orb_plane_line],eff_at_date['orbital_plane_decs'].iloc[0][ind_orb_plane_line],'-', lw = 1, color = 'k', label = 'CHEOPS orbital plane')
        if args.angle_max_roll > 0:
            # Plot zone around orbital plane where roll angle is max
            orb_plane_zone = mask_zone(RA, DEC, orb_plane_zone_init, cone_center = 'normal_orb_plane', start_zone = start_orb_plane_zone, end_zone = end_orb_plane_zone, day = eff_at_date['date'].iloc[0], single_target=False)
            orb_plane_zone_vis_cone = mask_zone(RA, DEC, orb_plane_zone, cone_center = 'sun', start_zone = args.SEA, end_zone = 180, day = eff_at_date['date'].iloc[0], single_target=False)
            ax_contourf.contourf(RA,DEC, orb_plane_zone_vis_cone, levels=[0.99,1], colors=['indianred','none'], hatches=['',''], label = 'zone close to the orbital plane', alpha = 0.3, zorder = 5)   

        ax.set_title(Time(eff_at_date['date'].iloc[0], format='jd', scale='utc').strftime('%d-%b'), weight = 'bold')
        ax_contourf.get_yaxis().set_ticks([])
        ax_contourf.set_yticklabels([])
        # # Plot individual targets location #
        if len(targets) < nb_max_targets :
            for id, target in targets.iterrows():
                target_name = target['target_name']
                RA_target = target['RA(deg)']
                Dec_target = target['DEC(deg)']
                ax_contourf.scatter(RA_target,Dec_target, marker = '*', s = 200, edgecolor='k', label = target_name, zorder = 8)    
            #ax_contourf.legend()
        else:
            print(f'\nMore than {nb_max_targets} targets, not showing them on visibility map.\n')
        # Adapt location of legend
        if (date > min_date + 100) & (date < min_date + 330): # From March/April to End November
            ax.legend(all_handles, all_labels, loc='upper center')
        else:
            ax.legend(all_handles, all_labels, loc='lower right')

    # Attach the update function to the slider
    date_slider.on_changed(lambda val: update(val, eff_contours))
    
def main(current_folder, default_values, args): 
    
    # Are we in ESA datalabs
    in_datalab = 'datalab' in socket.gethostname()
        
    # ARGUMENTS
    target_name = args.target_name 
    RA = args.RA 
    Dec = args.Dec 
    obs = args.obs
    mode = args.mode 
    targets_file = args.targets_file 
    sun_exclusion_angle = args.SEA
    angle_max_roll_rate = args.angle_max_roll
    hide_interactive_plot = args.hide_slider
    hide_plots = args.hide_plots
     
    # Fix directory for datalabs
    
    # DEFAULT VALUES
    min_SEA = default_values[0] # value for interpolation
    default_SEA = default_values[1]
    default_angle_max_roll = default_values[2]
    sw_version = default_values[3]
    files_version = default_values[4]
    current_year = default_values[5]
    
    ### Define grid arrays: ra, dec, time and sun angle
    interp_grid_size_deg = 1 # Grid size in RA/Dec 
    ra_grid = np.linspace(0, 360, int(360/interp_grid_size_deg))
    dec_grid = np.linspace(-90, 90, int(180/interp_grid_size_deg))
    dec_mesh, ra_mesh = np.meshgrid(dec_grid, ra_grid) # For later
    # Current year time and sun angle arrays
    start_date = Time(datetime(current_year,1,1), scale='utc').jd + 0.5 # Midday
    end_date = Time(datetime(current_year,12,31), scale='utc').jd + 0.5 # Midday
    dates = np.arange(start_date, end_date, 1)
    time_array = Time(dates, format='jd', scale='utc').jd
    # Get the sun position at these times
    sun_angle_grid = get_sun(Time(time_array, format='jd', scale='utc')).ra.value

    print(f'\n\n================ CHEOPS Efficiency and Visibility tool | version: {sw_version} | files version: V{files_version} ================\n')
    print('    |   ! This tool is designed to provide a first-order estimate of CHEOPS visibility, to fully         |')
    print('    |   judge the feasibility of CHEOPS observations, please use the Scheduling Feasibility Checker !    |\n\n')
       

    if sun_exclusion_angle == default_SEA:
        print(f' --- Sun exclusion angle set to {default_SEA} degrees (actual CHEOPS value)')
    else:
        print(f' --- Sun exclusion angle set to {sun_exclusion_angle} degrees (CHEOPS current SEA = {default_SEA})')
    
    if angle_max_roll_rate != 0:
        print(f' --- Maximum roll rate zone set to {angle_max_roll_rate} degrees from the orbital plane (CHEOPS value = {default_angle_max_roll})')
   
    single_target = True 
    ids_to_drop = []
    if target_name != None: # Single target mode
        if (RA == None) | (Dec == None): # No RA/Dec given, query
            try:
                coo = get_icrs_coordinates(target_name)
            except:
                print(f'Unresolved target_name \'{target_name}\'...\n')
                sys.exit(0)
            RA = coo.ra.value
            Dec = coo.dec.value
            print(f'\nNo RA/Dec provided, querying Seasme database for {target_name}: RA = {np.round(RA,3)} deg, Dec = {np.round(Dec,3)} deg')
        else: # RA/Dec given
            print(f'Using RA = {np.round(RA,3)} deg and Dec = {np.round(Dec,3)} deg for {target_name}')
    elif targets_file != None: # multi targets mode 
        if in_datalab:
            targets_file = os.path.join(os.environ.get('VIS_FOLDER'), targets_file) # This environment variable is set in the Dockerfile for the datalabs
        single_target = False
        print(f'Using list of targets in {targets_file}')
    elif (RA != None) & (Dec != None):
        print('Only RA and Dec given, setting name to: \'Unamed_target\'.')
        target_name == 'Unamed_target'
    else:
        print("Error: \'target_name\',  \'targets_file\' or RA/DEC arguments must be provided !\n")
        sys.exit(1)
    
    if single_target: # single target mode
        print(f'Computing efficiencies for {target_name}')
        target_data = {
            'target_name':[target_name],
            'RA(deg)':[RA],
            'DEC(deg)':[Dec],
            'obs(dd-Mon)':[obs]
        }
        targets = pd.DataFrame(target_data)
    else: # multiple targets
        targets = pd.read_csv(targets_file, skipinitialspace=True, sep=',|;', engine='python')
        # Validate entries 
        for id, target in targets.iterrows():
            valid, target = validity_file_entries(target, id)
            if not valid:
                ids_to_drop.append(id)
            else:
                targets.iloc[id] = target
    
    targets = targets.drop(ids_to_drop)  # Remove entry from targets if something is wrong     

    Gmags = np.array([6,12])
        
    if mode == None:
        print('!! No mode argument given, defaulting to bright mode, efficiencies are given assuming a target of brightness Gmag = 6.')
        Gmag_user = Gmags[0]
    elif mode == 'b':
        Gmag_user = Gmags[0]
        print('Bright mode, efficiencies are given assuming a target of brightness Gmag = 6.')
    elif mode == 'f':
        Gmag_user = Gmags[1]
        print('Faint mode, efficiencies are given assuming a target of brightness Gmag = 12.')
    else:
        print("Invalid mode value, must be 'b' (bright, Gmag = 6) or 'f' (faint, Gmag = 12), defaulting to bright mode, efficiencies are given assuming a target of brightness Gmag = 6.")
        Gmag_user = Gmags[0]
    
    # General parameters
    interp_files = [#'ra_grid',
                    #'dec_grid',
                    'interp_function_Gmag6',
                    'time_eff_interpolated_table_Gmag6',
                    'interp_function_Gmag12',
                    'time_eff_interpolated_table_Gmag12']
    interp_files = [s + f"_{current_year}_V{files_version}" for s in interp_files]
    eff_uncertainty = 3 # in %
    nb_targets = len(targets) # number of targets
    processing_folder = os.path.join(current_folder, 'processing_files') # where the star_obs, target_list, and interpolation objects (computed after first run) are stored
    # If we are running the Visibility tool in an ESA datalabs, then define a different path for the output folder
    if in_datalab:
        output_folder = os.path.join(os.environ.get('VIS_FOLDER'), 'output') # This environment variable is set in the Dockerfile for the datalabs
        matplotlib.use("TkAgg")  # Or "Qt5Agg" if you have Qt installed
    else:
        output_folder = os.path.join(current_folder, 'output') # folder for plots and summary file
    if single_target: 
        if targets['obs(dd-Mon)'].values[0] != '':
            day_print = targets['obs(dd-Mon)'].values[0].split('-')[0]
            month_print = targets['obs(dd-Mon)'].values[0].split('-')[1]
            output_file = os.path.join(output_folder, targets['target_name'].values[0] + '_efficiency_Gmag' + str(Gmag_user)+ '_' + day_print + '_' + month_print + '.csv') # summary file
        else:
            output_file = os.path.join(output_folder, targets['target_name'].values[0] + '_efficiency_Gmag' + str(Gmag_user)+ '.csv') # summary file

    else:
        output_file = os.path.join(output_folder, 'targets_efficiency_Gmag' + str(Gmag_user) + '.csv') # summary file
    
    # If output folder doesn't exist, create it.
    if os.path.exists(output_folder):
        pass
    else:
        os.mkdir(output_folder)
    with open(output_file, 'w') as file_out:
        file_out.write('target_name,RA(deg),DEC(deg), start_visibility, end_visibility, median_efficiency, max_efficiency, date_max, today_efficiency, date_today, planned_efficiency, date_planned\n')
    
    # Remove older versions of file in the folder
    for f in Path(processing_folder).iterdir():
        if f.suffix == '.txt':
            pass
        elif f.stem[-4:] == files_version:
            pass
        else:
            if f.exists() and f.is_file() and (('grid' in f.stem) or ('interp' in f.stem)):
                print(f"file: {f.stem} has the wrong version, deleting... ")
                f.unlink()
            

    # Check if the interpolation objects were already generated, if yes use them, otherwise do interpolation from scratch and generate the objects
    for file_name in interp_files:        

        file_path = os.path.join(processing_folder, file_name)
            
        if os.path.exists(file_path):
            pass
        else:
            print(f"Interpolation function and interpolation tables does not yet exist (or is from the wrong software version or the wrong year), starting interpolation from scratch and creating them ...")
            print(f'This can take up to a few minutes the first time the code is run, but only for the first target! It should only take ~1 second per target after that.')
            # MPS target_list file:
            MPS_target_lists = search_files(processing_folder, 'target_list')
            # MPS star_obs file:
            MPS_star_observations_vs_times = search_files(processing_folder, 'star_observations_vs_time') 
            if len(MPS_target_lists) != len(MPS_star_observations_vs_times):
                raise Exception("Not the same number of star_observations_vs_times and target_list files!")
            for i in range(len(MPS_target_lists)):
                target_list = MPS_target_lists[i]
                star_obs_vs_time = MPS_star_observations_vs_times[i]
                full_interp(processing_folder, target_list, star_obs_vs_time, Gmags[i], min_SEA, files_version, time_array, sun_angle_grid, ra_grid, dec_grid, current_year)
            print(f'\nInterpolation done!\n')
            break 
    
    print('Loading interpolated tables...')                
    print('\n==============================================================================================================\n')

    interp_fcn = []
    interp_data = []
    
    for Gmag in Gmags:
        ### First iteration of the code locally, create the objects for interpolation and store them in files in the 'processing_files' folder
        with open(os.path.join(processing_folder, 'interp_function_Gmag' + str(Gmag) + '_' + str(current_year) + '_V' + files_version), 'rb') as file:
            interp_fcn.append(pickle.load(file))
        
        with open(os.path.join(processing_folder, 'time_eff_interpolated_table_Gmag' + str(Gmag) + '_' + str(current_year) + '_V' + files_version), 'rb') as file:
            interp_data.append(pd.read_pickle(file))
        
    # with open(os.path.join(processing_folder, interp_files[0]), 'rb') as file:
    #     RA = pickle.load(file)
        
    # with open(os.path.join(processing_folder, interp_files[1]), 'rb') as file:
    #     DEC = pickle.load(file)
    
    ### Iterates through all targets
    for id, target in targets.iterrows():    
        target_name = target['target_name']
        RA_target = target['RA(deg)']
        Dec_target = target['DEC(deg)']
        planned_obs = target['obs(dd-Mon)']
        
        if (hide_plots == False) and (nb_targets <= 10):
            fig, ax = plt.subplots(figsize=(10,6))
            if angle_max_roll_rate > 0:
                # ax2 for the distance to orbital plane 
                ax2 = ax.twinx()
            
        for j, mag in enumerate(Gmags):
            
            full_interp_data = interp_data[j]                                                     
            interp_function = interp_fcn[j]
                        
            # Format time values
            today = datetime.now().replace(year=current_year, hour = 12, minute = 0, second = 0, microsecond=0)
            date_format = "%d-%b"
            if (planned_obs != '') & (planned_obs != None): # if the obs field is not empty
                planned_obs_dt = datetime.strptime(planned_obs, date_format).replace(year=current_year) + timedelta(hours = 12)
            time_array = full_interp_data['date'] # array of jd values
            time_object = Time(time_array, format='jd', scale='utc') # Astropy Time object
            dates = [t.datetime for t in time_object] # datetime object (used for plotting)
            
            # Define the grid for interpolation
            sun_angle_mesh, ra_target_mesh, dec_target_mesh = np.meshgrid(sun_angle_grid, RA_target, Dec_target, indexing='ij')
            # Interpolate
            target_eff = interp_function(sun_angle_mesh, ra_target_mesh, dec_target_mesh)[:,0,0]
            
            # Smooth the curve
            target_eff = gaussian_filter(target_eff, sigma=2)            
            
            # Check if we are in the visibility cone (i.e., not in the sun exclusion angle), otherwise, set values to 0
            for i,day in enumerate(time_array):
                target_eff[i] = mask_zone(RA_target, Dec_target, target_eff[i], cone_center = 'sun', start_zone = sun_exclusion_angle, end_zone = 180, day = day, single_target = True)
                

            # Add JWST visibility if requested
            JWST_vis_array = np.zeros(shape = np.shape(target_eff))
            for i,day in enumerate(time_array):
                JWST_vis_array[i] = check_JWST_visibility_single_target(RA_target, Dec_target, day)
            JWST_vis_ranges = find_JWST_vis_ranges(JWST_vis_array, dates)
        

            # Add distance to CHEOPS orbital plane
            angle_to_orb_plane_array = np.zeros(shape = np.shape(target_eff))
            for i,day in enumerate(time_array):
                angle_to_orb_plane_array[i] = get_orbital_plane_dist(RA_target, Dec_target, day)
        
            ### Display info
            if mag == Gmag_user:
                # Target non visible
                non_zero_eff_ind = np.where(target_eff > 0)[0] # non zero efficiency
                if len(non_zero_eff_ind) == 0: # never visible 
                    print(f'\n{target_name} (RA = {np.round(RA_target,3)}, Dec = {np.round(Dec_target,3)}), not visible with CHEOPS :(')
                    print('______________________________________________________________________________________________________________\n')                
                    continue # go to next target

                # visibility limits 
                ind_non_zero = np.where(target_eff > 0)[0]
                ind_zero = np.where(target_eff == 0)[0]            
                if (ind_non_zero[0] == 0) or (ind_non_zero[-1] == (len(target_eff)-1)): # start of visibility is before 31-Dec and end is the following year
                    vis_across_years = True
                    vis_x1 = ind_zero[0]
                    vis_x2 = ind_zero[-1]
                    start_vis = dates[np.where(np.array(dates) <= dates[vis_x2])[0][-1]+1].strftime(date_format)
                    end_vis = dates[np.where(np.array(dates) >= dates[vis_x1])[0][0]-1].strftime(date_format)
                else: # start of visibility is after 01-Jan and end is before 31-Dec
                    vis_across_years = False
                    vis_x1 = ind_non_zero[0]
                    vis_x2 = ind_non_zero[-1]
                    start_vis = dates[np.where(np.array(dates) >= dates[vis_x1])[0][0]].strftime(date_format)
                    end_vis = dates[np.where(np.array(dates) <= dates[vis_x2])[0][-1]].strftime(date_format)

                # efficiency today
                ind_today = np.where(dates == np.array(today))[0][0]
                eff_today = target_eff[ind_today]

                # efficiency at planned_obs
                if (planned_obs != '') & (planned_obs != None):  # if the obs field is not empty
                    ind_planned_obs = np.where(dates == np.array(planned_obs_dt))[0][0]
                    eff_planned_obs = target_eff[ind_planned_obs]

                # maximum efficiency max
                max_eff = np.nanmax(target_eff)
                ind_max_eff = np.where(target_eff == max_eff)[0]
                date_max_eff = (Time(time_array[ind_max_eff].values[0], format='jd', scale='utc').strftime(date_format))
                
                # median non zero eff
                med_non_zero_eff = np.nanmedian(target_eff[non_zero_eff_ind])
                
                # display info
                print(f'\n{target_name} (RA = {np.round(RA_target,3)}, Dec = {np.round(Dec_target,3)}), visible from {start_vis} to {end_vis}: (efficiencies computed using GMag = {Gmag_user})')
                print(f'    Median efficiency: {int(med_non_zero_eff)} %')#+/-{eff_uncertainty} %')
                print(f'    Maximum efficiency: {int(max_eff)} % (reached on {date_max_eff})')
                print(f'    Today\'s efficiency ({today.strftime(date_format)}): {int(eff_today)} %')
                if (planned_obs != '') & (planned_obs != None):
                    print(f'    Efficiency on {planned_obs_dt.strftime(date_format)}: {int(eff_planned_obs)} %')#+/-{eff_uncertainty} %')
                # if JWST_vis:
                JWST_str = f"    {target_name} is visible with JWST from "
                n = 0
                for start, end in JWST_vis_ranges:
                    if n > 0:
                        JWST_str += " and from "
                    JWST_str += f"{start.strftime(date_format)} to {end.strftime(date_format)}"
                    n += 1
                print(JWST_str)
                print('______________________________________________________________________________________________________________\n')
                
                # Write out info
                with open(output_file, 'a') as file_out:
                    if (planned_obs != '') & (planned_obs != None):             
                        str_out = '%s,%.3f,%.3f,%s,%s,%d,%d,%s,%d,%s,%d,%s\n'%(target_name,RA_target,Dec_target,start_vis,end_vis,int(med_non_zero_eff), int(max_eff), date_max_eff, int(eff_today), today.strftime(date_format), int(eff_planned_obs), planned_obs_dt.strftime(date_format))
                    else:
                        str_out = '%s,%.3f,%.3f,%s,%s,%d,%d,%s,%d,%s,,\n'%(target_name,RA_target,Dec_target,start_vis,end_vis,int(med_non_zero_eff), int(max_eff), date_max_eff, int(eff_today), today.strftime(date_format)) 
                    file_out.write(str_out)

            ### Plots
            
            if (hide_plots == True) or (nb_targets > 10):
                continue
    
            # Plot 1: Efficiency vs time plot
            
            if mag == Gmag_user:
                ax.axvline(today, color = 'olive', linewidth = 2, label = f'Today: {today.strftime(date_format)}', alpha = 0.7)
                ax.axhline(50, color = 'k', linewidth = 1, alpha = 0.7,linestyle = '-')
                ax.plot(dates, target_eff,  '-', linewidth = 2, color = 'teal',alpha = 0.7, label = f'Gmag = {mag}')
                if (planned_obs != '') & (planned_obs != None):
                    ax.axvline(planned_obs_dt, color = 'purple', linewidth = 2, alpha = 0.7, label = f'Planned obs: {planned_obs_dt.strftime(date_format)}')
                # shaded area 
                if vis_across_years:
                    ax.fill_between(dates, 0, 100, where=(np.array(dates) >= dates[vis_x1]) & (np.array(dates) <= dates[vis_x2]) , color='darkgrey', alpha=0.6, label='Target non visible')
                else:
                    ax.fill_between(dates, 0, 100, where=(np.array(dates) >= dates[vis_x2]) | (np.array(dates) <= dates[vis_x1]) , color='darkgrey', alpha=0.6, label='Target non visible')
                    
                # if JWST_vis:
                n = 0
                for start, end in JWST_vis_ranges:
                    if n > 0:
                        ax.axvspan(start, end, ymin=0, ymax=100, color='seagreen', alpha=0.3, zorder = 0)
                    else:
                        ax.axvspan(start, end, ymin=0, ymax=100, color='seagreen', alpha=0.2, label = 'JWST visibility', zorder = 0)
                    n += 1
            
                # Add second y-axis to plot the distance to orbital plane
                if angle_max_roll_rate > 0:
                    if vis_across_years:
                        ax2.plot(dates[:vis_x1], angle_to_orb_plane_array[:vis_x1], '-', color = 'brown', linewidth = 1, alpha = 0.8, label = 'Distance to orbital plane (degrees)')
                        ax2.plot(dates[vis_x2:], angle_to_orb_plane_array[vis_x2:], '-', color = 'brown', linewidth = 1, alpha = 0.8)

                    else:
                        ax2.plot(dates[vis_x1:vis_x2], angle_to_orb_plane_array[vis_x1:vis_x2], '-', color = 'brown', linewidth = 1, alpha = 0.8, label = 'Distance to orbital plane (degrees)')
                    
                    ax2.fill_between(dates, 0, angle_max_roll_rate, color='brown', alpha=0.2, label="Maximum roll angle rate zone", zorder = 0)
                    ax2.set_ylim([0,90]) 
                    ax2.set_ylabel("Distance from target to orbital plane (degrees)", color="brown", weight = 'bold')
                    ax2.tick_params(axis='y', labelcolor="brown")
                    ax2.legend(loc='upper right')
            else:
                ax.plot(dates, target_eff,  '--', linewidth = 2, color = 'teal', alpha = 0.7, label = f'Gmag = {mag}', zorder = 0)

                          
            # x-axis
            ax.xaxis_date()  # Tell Matplotlib that the x-axis is in datetime format
            ax.xaxis.set_major_formatter(plt.matplotlib.dates.DateFormatter(date_format))
            ax.set_xlim([dates[0],dates[-1]])
            
            # y-axis
            ax.tick_params(axis='y', which='both', right=False, labelcolor="teal")
            ax.yaxis.set_minor_locator(plt.MultipleLocator(5))
            ax.set_ylim([0,100])#np.max(target_eff)+2])
            ax.set_ylabel(f'Efficiency (%) over 7 orbits (~12 hours)', color="teal", weight = 'bold')
            ax.set_title(f'{target_name}: RA = {np.round(RA_target,3)} [deg], Dec = {np.round(Dec_target,3)} [deg]' )
            ax.legend(loc='upper left')
            
            
            if planned_obs != '':
                day_print = planned_obs.split('-')[0]
                month_print = planned_obs.split('-')[1]
                fig.savefig(os.path.join(output_folder, f'Efficiency_{target_name}_{day_print}_{month_print}.png'), format = 'png')
            else:
                fig.savefig(os.path.join(output_folder, f'Efficiency_{target_name}.png'), format = 'png')

    ind_user = np.where(Gmags == Gmag_user)[0][0]
    full_interp_data = interp_data[ind_user]    
    
    if not hide_interactive_plot:
        interactive_plot(current_folder, args, full_interp_data, ra_mesh, dec_mesh, targets, eff_uncertainty, Gmag_user)
    else:
        pass
    
    plt.show()
