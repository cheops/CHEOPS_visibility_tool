<h1 align="center">
  <br>
  CHEOPS Efficiency and Visibility tool
  <br>
</h1>

<h4 align="center">
  <a href="https://www.cosmos.esa.int/web/cheops-guest-observers-programme/">--> CHEOPS webpage <--</a>
</h4>

![image](misc_files/visibility.png)

## What is CHEOPS' efficiency?

Due to it's low Earth orbit, CHEOPS observations may be interrupted by some of the following events, which will lead to gaps in the light curves:

-  <u>South Atlantic Anomaly (SAA) crossing</u>: Due to the very high number of particules hitting CHEOPS' CCD while flying through this weaker region of Earth's magnetic field, images obtained during these crossings are discarded.
-  <u>Earth occultations</u>: When the Earth is in the line of sight, the target is not visible and those images are discarded on-board.
-  <u>Earth's stay-light</u>: When the line of sight is grazing Earth's atmosphere, reflected light from the Sun degrades the CHEOPS images and those images are discarded.
  
The efficiency is defined as the total duration of a visit divided by the on-source time. In other words, it is the fraction of the visit that actually contains images.

The efficiency estimate output by this tool is based on a standardised visit for different RA/Dec/time. The duration of these visits is 7 orbits (~12h), the time required for CHEOPS to come back to the same longitude, therefore including all the above described effects.

## CHEOPS' visibility

To maintain its thermal stability, CHEOPS’ line-of-sight must remain within 60 degrees of the anti-Sun location, i.e. CHEOPS has a Sun exclusion angle of 120 degrees. Some targets are therefore not accessible at all to CHEOPS (low/high declinations) and other targets are only visible during certain seasons each year. At the end of 2024, the Sun Exclusion angle was relaxed to 117 degrees in order to extend the pool of visible stars.

## Orbital plane and maximum roll rate

When pointing closer than 10 degrees (as of 05 Dec 2024) from CHEOPS' orbital plane, a maximum roll rate is imposed to avoid the spacecraft to roll too fast in order to maintain his nadir locking. Both the location of the orbital plane and the zone where the maximum roll rate is enforced can be shown on the visibility plot using the arguments 'orb_plane' and 'angle_max_roll', respectively. Due to the high roll rate, targets in this zone might periodically (per orbit) see there pointing uncertainty increase up to possibly ~10" for a few minutes. We advise users with targets in this zone to check any possible resulting impact in their lightcurves. 

## Disclaimer

The efficiency retrieved using this software is indicative. To prepare CHEOPS observations, including e.g. critical phase ranges or precise timing in borderline cases, please make use of the Schedule Feasibility Checker, available <a href="https://gitlab.unige.ch/cheops/containers/cheops-sfc-webtop">here</a>.


# Installation

The only requirment is [Python](https://www.python.org/downloads/). From your command line, run the following:

```bash
# Clone this repository
$ git clone https://gitlab.unige.ch/cheops/CHEOPS_visibility_tool/

# Go into the repository
$ cd CHEOPS_visibility_tool

###########################################################################
# Before installing dependencies for which minimal versions are required, #
# we strongly recommend the user to work in a python virtual environment  #
###########################################################################
# Using pip 
$ python3 -m venv /path/to/new/virtual/environment # Create your venv
$ source <venv>/bin/activate  # Activating on Linux/MAC OSX
$ <venv>\Scripts\activate.bat # Activating on WINDOWS via cmd.exe
$ <venv>\Scripts\Activate.ps1 # Activating on WINDOWS via PowerShell
# Using conda 
$ conda create -n yourenvname python=x.x
$ conda activate yourenvname
###########################################################################

# Install dependencies
$ pip install -r requirements.txt
```

## Running the code
 
#### 1. Command line

```bash 
# 1. For a single target, run (RA/Dec are optional, see definitions of command line arguments below)
$ python3 mk_CHEOPS_eff_maps.py --target_name TARGET_NAME --RA RA --Dec DEC --obs OBS --mode mode --SEA SEA --orb_plane --angle_max_roll ANGLE_MAX_ROLL --hide_slider --hide_plots

# 2. For multiple targets, use the targets_file argument (definition below), run
$ python3 mk_CHEOPS_eff_maps.py --targets_file PATH_TO_YOUR_TARGETS_FILE --mode mode --SEA SEA --orb_plane --angle_max_roll ANGLE_MAX_ROLL --hide_slider --hide_plots 
```
Command line arguments:

```
1. In single target mode, --target_name must be given. If --RA or --Dec is not provided, then the target coordinates will be queried from Seasame using --target_name.
2. In multiple target mode, --targets_file must be given as 'your_file.csv'. This comma separated file contains one row per target and the only mandatory column for each target is target_name. See example file 'targets_example_file.csv'

target_name: Target ID that will be queried to Sesame if no --RA or --Dec is given. This ID will also be the one displayed on the plots.
RA: Target Right Ascension [0:360] (degrees) 
Dec: Target Declination [-90:90] (degrees) 
obs: What day is the observation planned? Used to display the planned efficiency at that date on the plot (str with the format 'DD-Mmm', e.g. '25-Jan')
mode: 'b' for bright or 'f' for faint. In bright/faint mode, the output efficiencies are derived from a star of Gmag = 6 / Gmag = 12.
targets_file: path of a csv file containing your targets. Columns are 'target_name,RA(deg),DEC(deg),obs(dd-Mon)' with one row per target. See example in 'targets_example_file.csv'.
SEA: Sun Exclusion Angle [110:120] (degrees). CHEOPS can only observe target present in a cone of radius (180-SEA) from the antisun position. Current value is 117 (as of February 2025).
orb_plane: If present, this argument will show the location on the sky of the orbital plane.
ANGLE_MAX_ROLL: If present, shows the intersection between the visibility cone and a zone of X degrees away from the orbital plane, in which the maximum roll rate is enforced. X is the current value in the CHEOPS inflight software if no values are given for this argument, otherwise, the given value is used.
hide_slider: If present, hides the interactive visibility plot.
hide_plots: If present, hides individual efficiency plot (one per target, limited to 10 targets).
```


## Outputs

In the ``output`` folder, you will find an efficiency vs time plot for each input targets and a ``targets_efficiency_GmagX.csv`` file (one per target on single target mode, and one for all target on multiple targets mode), with all the efficiency and visibility info for each targets.

## Credits

This software uses the following open source packages:

- [Numpy](https://numpy.org/)
- [Scipy](https://scipy.org/) >= 1.10.0
- [Astropy](https://www.astropy.org/) >= 5.3
- [Pandas](https://pandas.pydata.org/) >= 2.0.0
- [Matplotlib](https://matplotlib.org/)

Developped by the CHEOPS Science Operation Center:

- [Alexis Heitzmann](mailto:alexis.heitzmann@unige.ch)
- [Nicolas Billot](mailto:nicolas.billot@unige.ch)
- [Anja Bekkelien](mailto:anja.bekkelien@unige.ch)
- [Adrien Deline](mailto:adrien.deline@unige.ch)

#

<img src="misc_files/CHEOPS.png" width="200">
